/** Programme de jeux Connect4 (Puissance 4)
 * 
 * Le but du jeu est d'aligner une suite de 4 pions de même type (X ou O) sur une grille comptant 6 rangées et 7 colonnes.
 * Tour à tour les deux joueurs placent un pion dans la colonne de leur choix, le pion coulisse alors jusqu'à la position 
 * la plus basse possible dans la dite colonne à la suite de quoi c'est à l'adversaire de jouer. Le vainqueur est le joueur 
 * qui réalise le premier un alignement (horizontal, vertical ou diagonal) consécutif d'au moins quatre pions de son type. 
 * Si, alors que toutes les cases de la grille de jeu sont remplies, aucun des deux joueurs n'a réalisé un tel alignement, 
 * la partie est déclarée nulle.
 * 
 * @author Marc Potvin (POTM28116800)
 * @version 1.0 
 * @date 2018-10-04
 */

#include <stdio.h>
#include <stdlib.h>

#define R 6
#define C 7

/**
 * matrice pour le plateau de jeu
 */
char board[R][C] = { {' ',' ',' ',' ',' ',' ',' '},\
                     {' ',' ',' ',' ',' ',' ',' '},\
                     {' ',' ',' ',' ',' ',' ',' '},\
                     {' ',' ',' ',' ',' ',' ',' '},\
                     {' ',' ',' ',' ',' ',' ',' '},\
                     {' ',' ',' ',' ',' ',' ',' '} };

/**
 * fonction qui dessine le plateau de jeu
 */
void drawBoard();

/**
 * fonction qui vide l'entrée standard 
 */
void clear_stream();

/**
 * fonction qui vérifie si la colonne est pleine 
 * @param int action - l'entrée de l'utilisateur
 * @return int - retourne 1 si la colonne est pleine, sinon retourne 0
 */
int isColumnFull(int action);

/**
 * fonction qui obtient la saisie de l'utilisateur et vérifie si elle est valide
 * @param int *action - pointeur sur la variable d'entrée utilisateur
 * @param char player - le joueur actuel, X ou O
 */
void getAction(int *action, char player);

/**
 * fonction qui exécute le mouvement des joueurs
 * @param int action - la valeur d'entrée 
 * @param char player - le joueur actuel, X ou O
 */
void doAction(int action, char player);

/**
 * fonction qui vérifie pour une victoire
 * @param char player - le joueur actuel, X ou O
 * @return int - retourne 1 s'il y a une victoire, sinon 0
 */
int checkForWin(char player);

/**
 * fonction qui vérifie si toutes les colonnes sont pleines
 * @return int - retourne 1 s'il y a un match nul, sinon 0
 */
int checkForDraw();

int main(int argc, char *argv[]){
    char player = 'X';
    int action;
    int end=0;
    if (argc == 1) {
        while(end==0){
            drawBoard();
            printf("\nPlayer %c action: ", player);
            getAction(&action, player);
            printf("\n");
            doAction(action, player);
            if(checkForWin(player)==1){
                drawBoard();
                end=1;
                printf("\nPlayer %c wins!\n", player);
                exit(0);
            }
            if(checkForDraw()==1){
                drawBoard();
                end=1;
                printf("\nIt's a draw...\n");
                exit(0);
            }
            player=(player=='X') ? 'O' : 'X';
        }
    }else{
        fprintf( stderr,"Error: No argument expected.\n");
        exit(1);
    }
}

void drawBoard(){
    int i;
    int j;
    printf("\n  1 2 3 4 5 6 7 \n");
    for(i=0;i<R;i++){
        printf("| ");
        for(j=0;j<C;j++){
            printf("%c ", board[i][j]);
        }
        printf("|\n");
    }
    printf("-----------------\n");

}

void clear_stream(){
    char ch;
    while ((ch = getchar()) != '\n' && ch != EOF); 
}

int isColumnFull(int action){
    int isFull=0;
    switch(action) {
      case 1 :
        if(board[0][0]=='X'||board[0][0]=='O')
         	isFull=1;
        break;
      case 2 :
      	if(board[0][1]=='X'||board[0][1]=='O')
         	isFull=1;
         break;
      case 3 :
      	if(board[0][2]=='X'||board[0][2]=='O')
         	isFull=1;
         break;
      case 4 :
      	if(board[0][3]=='X'||board[0][3]=='O')
         	isFull=1;
         break;
      case 5 :
      	if(board[0][4]=='X'||board[0][4]=='O')
         	isFull=1;
         break;
      case 6 :
      	if(board[0][5]=='X'||board[0][5]=='O')
         	isFull=1;
         break;
       case 7 :
      	if(board[0][6]=='X'||board[0][6]=='O')
         	isFull=1;
         break;
   }
    return isFull;
}

void getAction(int *action, char player){
    while (scanf("%d", action) != 1 || *action<0||*action>7) {
        clear_stream();
        fprintf( stderr,"Error: action should be an integer between `1` and `7`.\n");
        printf("Player %c action: ", player);
        fflush(stdout);
    }
    if(*action==0){
        printf("exit\n");
        exit(0);
    }
    if (isColumnFull(*action)==1) {
        fprintf( stderr,"Error: column `%d` is full.\n", *action);
        printf("Player %c action: ", player);
        getAction(action, player);
    }
}

void doAction(int action, char player){
    int j;
    if(action==1){
        j=R-1;
        while(board[j][0]=='X'||board[j][0]=='O'){
            j--;
        }
        board[j][0]=player;
    }else if(action==2){
        j=R-1;
        while(board[j][1]=='X'||board[j][1]=='O'){
            j--;
        }
        board[j][1]=player;
    }else if(action==3){
        j=R-1;
        while(board[j][2]=='X'||board[j][2]=='O'){
            j--;
        }
        board[j][2]=player;
    }else if(action==4){
        j=R-1;
        while(board[j][3]=='X'||board[j][3]=='O'){
            j--;
        }
        board[j][3]=player;
    }else if(action==5){
        j=R-1;
        while(board[j][4]=='X'||board[j][4]=='O'){
            j--;
        }
        board[j][4]=player;
    }else if(action==6){
        j=R-1;
        while(board[j][5]=='X'||board[j][5]=='O'){
            j--;
        }
        board[j][5]=player;
    }else if(action==7){
        j=R-1;
        while(board[j][6]=='X'||board[j][6]=='O'){
            j--;
        }
        board[j][6]=player;
    }
}

int checkForWin(char player){
    int i;
    int j;
    int retWin=0;
    // horizontal 
    for (j = 0; j<R-3 ; j++ ){
        for (i = 0; i<C; i++){
            if (board[i][j] == player && board[i][j+1] == player && board[i][j+2] == player && board[i][j+3] == player){
                retWin=1;
            }           
        }
    }
    // verticale
    for (i = 0; i<R-3 ; i++ ){
        for (j = 0; j<C; j++){
            if (board[i][j] == player && board[i+1][j] == player && board[i+2][j] == player && board[i+3][j] == player){
                retWin=1;
            }           
        }
    }
    // diagonale ascendante 
    for (i=3; i<R; i++){
        for (j=0; j<C-3; j++){
            if (board[i][j] == player && board[i-1][j+1] == player && board[i-2][j+2] == player && board[i-3][j+3] == player)
                retWin=1;
        }
    }
    // diagonale descendante
    for (i=3; i<R; i++){
        for (j=3; j<C; j++){
            if (board[i][j] == player && board[i-1][j-1] == player && board[i-2][j-2] == player && board[i-3][j-3] == player)
                retWin=1;
        }
    }
    return retWin;
}

int checkForDraw(){
    int retDraw=0;
    if((board[0][0]=='X'||board[0][0]=='O')&&(board[0][1]=='X'||board[0][1]=='O')&&(board[0][2]=='X'||board[0][2]=='O')&&\
        (board[0][3]=='X'||board[0][3]=='O')&&(board[0][4]=='X'||board[0][4]=='O')&&(board[0][5]=='X'||board[0][5]=='O')&&\
        (board[0][6]=='X'||board[0][6]=='O')){
       retDraw=1; 
    }
    return retDraw;

}

