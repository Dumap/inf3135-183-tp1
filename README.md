# Travail pratique 1 : `connect4`

Programme de jeux Connect4 (Puissance 4)


Auteur: Marc Potvin

Code permanent: POTM28116800

## Objectif

Le but du jeu est d'aligner une suite de 4 pions de même type (X ou O) sur une grille comptant 6 rangées et 7 colonnes.

## Présentation de la grille

Le jeu se joue dans une grille comptant 6 rangées de 7 colonnes.
La grille est affichée dans la sortie standard (`stdout`) selon la représentation suivante:

~~~

  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|               |
-----------------

~~~

## Tours de jeu

Tour à tour les deux joueurs placent un disque dans la colonne de leur choix,
le disque tombe alors jusqu'à la position la plus base possible dans la dite colonne
à la suite de quoi c'est à l'adversaire de jouer.

Dans le terminal, les couleurs des joueurs sont représentées par les caractères suivants:

* Joueur 1: `X`
* Joueur 2: `O`

Le programme affiche la grille puis invite le joueur `X` à saisir la colonne dans
laquelle il veut placer son disque:

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|               |
-----------------

Player X action:
~~~

Le joueur `X` saisit alors sa colonne grâce à l'entrée standard (`stdin`).
Par exemple `3`.

Puis c'est au joueur `O` de jouer.
Le programme affiche la grille mise à jour et invite le joueur `O` à saisir sa colonne:

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|     X         |
-----------------

Player O action:
~~~

Le joueur `O` saisit alors sa colonne grâce à l'entrée standard (`stdin`).
Par exemple `4`.

Puis c'est au joueur `X` de jouer.

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|     X O       |
-----------------

Player X action: 4


  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|       X       |
|     X O       |
-----------------

Player O action:
~~~

Et ainsi de suite...

## Fin du jeu

Le jeu termine dés que l'une des conditions suivantes est rencontrée:

* Un des deux joueurs saisit la colonne `0`
* Un des deux joueurs parvient à aligner 4 disques
* La grille est pleine

### Quitter le jeu

Afin de permettre aux joueurs de quitter la partie, une colonne spéciale `0` peut
être saisie lors de la phase d'action.

Ainsi, si le joueur tape 0, le programme affiche `exit` et termine:

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|               |
-----------------

Player X action: 0
exit
~~~

### Victoire

Pour gagner, le joueur doit aligner 4 disques de son type dans la grille.
Les disques peuvent être alignés horizontalement, verticalement ou en diagonale.

Dans l'exemple suivant, le joueur `X` gagne grâce à 4 disques alignés horizontalement.
Le programme affiche alors `Player X wins!` et termine.

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
| O O O         |
| X X X X       |
-----------------

Player X wins!
~~~

Ici, le joueur `X` gagne grâce à 4 disques alignés verticalement:

~~~
  1 2 3 4 5 6 7
|               |
|               |
| X             |
| X O           |
| X O           |
| X O           |
-----------------

Player X wins!
~~~

Il faut aussi prendre en compte les diagonales:

~~~
  1 2 3 4 5 6 7
|               |
|               |
| O             |
| X O           |
| O O O         |
| X X X O X X   |
-----------------

Player O wins!
~~~

### Grille pleine

Si la grille est pleine, plus aucune action n'est possible.
Le programme affiche alors la grille pleine suivie de `It's a draw...` puis termine.

~~~
  1 2 3 4 5 6 7
| O O O X O O O |
| X X X O X X X |
| O O O X O O O |
| X X X O X X X |
| O O O X O O O |
| X X X O X X X |
-----------------

It's a draw...
~~~

## Cas d'erreurs gérés

1. Si l'utilisateur fournit des arguments au programme, le message
   d'erreur suivant est affiché:
   ```sh
   Error: No argument expected.
   ```

2. Si l'utilisateur fournit un numéro de colonne invalide, le message d'erreur
   suivant est affiché:
   ```sh
   Error: action should be an integer between `1` and `7`.
   ```

3. Si l'utilisateur sélectionne une colonne déjà pleine le message d'erreur
   suivant est affiché::
   ```sh
   Error: the column `X` is full.
   ```
   où `X` est à remplacer par le numéro de la colonne.

## Comment jouer

Tout d'abord, vous devez compiler le programme. Pour compiler le programme, vous devez être dans le répertoire racine du projet et exécuter la commande suivante:
```sh
make bin/connect4
```
Ceci supprimera le répertoire bin s'il existe et le recréera en ajoutant l'objet exécutable `connect4`.

Une fois que vous avez votre exécutable, vous devez simplement l'appeler avec la commande suivante:
```sh
./bin/connect4
```
